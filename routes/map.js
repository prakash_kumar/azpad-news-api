﻿var geocode = require('geocoder');

exports.show = function (req, res) {

    res.render('maps', {
        key: process.env.API_KEY
    });
}

exports.geocode = function (req, res) {
    var lat = req.query.lat;
    var lng = req.query.lng;
    geocode.reverseGeocode(lat, lng, function (err, data) {
        if (err) res.end('error');
        if (data == null) {
            res.end('The lost city of atlantis');
        }else if (data.results.length == 0) {
            res.end('The lost city of atlantis');
        }
        else {
            var address = data.results[0].formatted_address;
            var country = address.substring((address.lastIndexOf(',') + 1));
            country = country.replace(/\s/g, '');
            if (country == "USA") { country = 'United States'; }
            res.end(country);
        }

    });
}
