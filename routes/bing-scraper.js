﻿var request = require("request");
var htmlparser = require('htmlparser');
var async = require('async');

exports.list = function (req, res) {
    var id = req.params.id;
    var url = 'http://www.bing.com/news/search?q=' + id + '&qs=n&form=NWBQBN&pq=' + id + '&sc=8-7&sp=-1&sk=';
    bing_search(url, function (jsonArray) {
        res.send(jsonArray);
    });

}

exports.timetable = function (req, res) {
    var id = req.params.id;
    var searchParams = req.query.size;
    var url = 'http://www.bing.com/news/search?q=' + id + '&first=' + (searchParams - 1) + '1&FORM=PONR';
    timetableArray = [];
    a = 0;
    while (a < searchParams) {
        bing_search(url, function (jsonArray) {
            timetableArray = timetableArray.concat(jsonArray);
            a++;
        });
        if ((a+1) == searchParams) {
            res.send(timetableArray);
        }
    }

}



function bing_search(url, callback) {
    request({ uri: url }, function (error, response, body) {
        if (error) res.send({ message: 'failed converting' });
        
        
        body = replaceAll(body, ' tm_fre', '');
        body = replaceAll(body, '&nbsp;&#0183;&#32;', '');
        body = replaceAll(body, '</div>', '');
        body = replaceAll(body, '</span>', '');
        body = replaceAll(body, 'sn_clst ', '');
        body = replaceAll(body, '<strong>', '');
        body = replaceAll(body, '</strong>', '');

        //find each part of urls from bing html, because of the carousel bar being annoying.
        var str_beginning = body.search('<div class="NewsResultSet clearfix">');
        var str_end = body.search('<div id="CarouselbarV2_1Title">More from Bing News');
        var body1 = body.substring(str_beginning + 36, str_end);
        var bodyonecount = occurrences(body1, '<div class="sn_r">', false);
        var str_end2 = body.search('<div class="RelatedSearchesAndActions">');
        var body2 = body.substring(nth_occurrence(body, '<div class="sn_r">', bodyonecount + 2), str_end2);
        body = body1 + body2;
        
        function escapeRegExp(string) {
            return string.replace(/([.*+?^=!:$ { }()|\[\]\/\\])/g, "\\$1");   
        }
        function replaceAll(string, find, replace) {
            return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);   
        }
        //look for tm_fres, closing spans, and more.
        console.log(body);

        console.log(body);
        var jsonArray = [];
        var count = occurrences(body, '<div class="sn_r">', false);
        console.log(count);
        for (var i = 0; i < count; i++) {
            var str_link = body.substring(body.search('<a href=') + 9, body.search('target') - 2);
            var str_title = body.substring(nth_occurrence(body, '>', 3) + 1, body.search('</a>'));
            var str_snip = body.substring(body.search('<span class="sn_snip">') + 22, body.search('<span class="sn_ST"'));
            var str_date = body.substring(body.search('<span class="sn_tm">') + 20,nth_occurrence(body,'<div class="sn_r">',2));
            var obj = { link: str_link, title: str_title, snip: str_snip, date: str_date };
            jsonArray.push(obj);
            body = body.substring(nth_occurrence(body, '<div class="sn_r">', 2));
            if (i == count - 1) {
                callback(jsonArray);
            }
        }
    });

}

function nth_occurrence(str, pat, n) {
    var L = str.length, i = -1;
    while (n-- && i++ < L) {
        i = str.indexOf(pat, i);
    }
    return i;
}
function occurrences(string, subString, allowOverlapping) {
    
    string += ""; subString += "";
    if (subString.length <= 0) return string.length + 1;
    
    var n = 0, pos = 0;
    var step = (allowOverlapping)?(1):(subString.length);
    
    while (true) {
        pos = string.indexOf(subString, pos);
        if (pos >= 0) { n++; pos += step; } else break;
    }
    return (n);
}

function cut(string, start, end) {
    return string.substring(0, start) + string.substring(end);
}

