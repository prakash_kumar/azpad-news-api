﻿Screenon = 0;
var map, infobox, marker;
function initialize() {
    var mapOptions = {
        zoom: 2,
        center: new google.maps.LatLng(26.2, 17.3)
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    google.maps.event.addListener(map, "click", function (event) {
        if (infobox != null)
            infobox.setVisible(false);
        //document.getElementById("lat").innerHTML = event.latLng.lat();
        //document.getElementById("lng").innerHTML = event.latLng.lng();
        Screenon = 0;
        //var url ='http://aznews.azurewebsites.net/api/map/?lat='+event.latLng.lat()+'&lng='+event.latLng.lng();
        var url = 'http://localhost:1337/api/map/?lat=' + event.latLng.lat() + '&lng=' + event.latLng.lng();
        //document.getElementById('lat').innerHTML = url;
        $.get(url, function (data, status) {
            $('#country').text(data);
            //var getRequestUrl = 'http://aznews.azurewebsites.net/api/bing/' + data;
            var getRequestUrl = 'http://localhost:1337/api/bing/' + data;
            $.getJSON(getRequestUrl, function (news) {
                var items = [];
                infobox = new InfoBox({
                    content: document.getElementById("infobox"),
                    disableAutoPan: false,
                    maxWidth: 150,
                    pixelOffset: new google.maps.Size(-140, 0),
                    zIndex: null,
                    boxStyle: {
                        background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
                        opacity: 0.75,
                        width: "280px"
                    },
                    closeBoxMargin: "12px 4px 2px 2px",
                    closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                    infoBoxClearance: new google.maps.Size(1, 1)
                });
                marker = new google.maps.Marker({ map: map, position: event.latLng, visible: false });
                infobox.open(map, marker);
                infobox.setVisible(true);
                map.panTo(new Latlng(marker.position.lat(), marker.position.lng() - 30));
                if (Screenon === 0) {
                    $('#title').text(news[0].title);
                    $('#snip').text(news[0].snip);
                    document.getElementById('url').innerHTML = ('<a href="' + news[0].link + '" target="_blank">Read More...</a>');
                    $('#date').text(news[0].date);
                }
                update = function (int) {
                    $('#title').text(news[int].title);
                    $('#snip').text(news[int].snip);
                    document.getElementById('url').innerHTML = ('<a href="' + news[int].link + '" target="_blank">Read More...</a>');
                    $('#date').text(news[int].date);
                }


            });
        });
    });
}
$('#forwards').click(function () {
    Screenon++;
    update(Screenon);
});

$('#backwards').click(function () {
    Screenon--;
    update(Screenon);
});
google.maps.event.addDomListener(window, 'load', initialize);